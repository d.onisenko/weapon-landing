const checkbox = document.querySelector("#theme-switch-checkbox");

checkbox.addEventListener("change", () => {
    document.body.classList.toggle("dark");
});
