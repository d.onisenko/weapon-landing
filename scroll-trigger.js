function scrollTrigger(selector) {
    let els = document.querySelectorAll(selector);
    els = Array.from(els);
    els.forEach((el) => {
        addObserver(el);
    });
}
function addObserver(el) {
    // We are creating a new IntersectionObserver instance
    let observer = new IntersectionObserver((entries, observer) => {
        // This takes a callback function that receives two arguments: the elements list and the observer instance.
        entries.forEach((entry) => {
            // `entry.isIntersecting` will be true if the element is visible
            if (entry.isIntersecting) {
                entry.target.classList.add("visible-on-scroll");
                observer.unobserve(entry.target);
            }
        });
    });
    // Adding the observer to the element
    observer.observe(el);
}
// Example usage
scrollTrigger(".client-header-container");
scrollTrigger(".client-text-container");
scrollTrigger(".info-container");
scrollTrigger(".social-media-header-container");
